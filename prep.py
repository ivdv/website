#! /usr/bin/env python3

import csv
from datetime import datetime

DAYS_SR = ["PON", "UTO", "SRE", "ČET", "PET", "SUB", "NED"]
DAYS_EN = ["MON ", "TUE", "WED", "THU", "FRI", "SAT", "SUN"]

def load_events(csv_path:str) -> list[dict]:
    events = []
    with open(csv_path) as csv_file:
        csv_reader = csv.reader(csv_file)
        next(csv_reader, None)
        for event in csv_reader:
            event_date = event[0]
            event_date_parsed = datetime.strptime(event_date, "%d-%m-%Y").date()
            event_time = event[1]
            event_location = event[2]
            event_title = event[3]
            current_event = {"date":event_date_parsed,
                             "time":event_time,
                             "location": event_location,
                             "title":event_title.strip()}
            events.append(current_event)
        return events

def build_html(events: list[dict], dayNames: list[str]) -> str:
    events_html = []
    for event in events:
        title = event["title"]
        location = event["location"]
        date = event["date"]
        date = dayNames[date.weekday()]+", "+str(date.day)+". "+str(date.month)+". "+str(date.year)+", "
        time = event["time"]+"h"
        event_html = []
        event_html.append(f"<div class='date'>{date} {time}</div>")
        event_html.append(f"<div class='title'>{title}</div>")
        if "https://" in location:
            place,link = location.split("https://")
            event_html.append(f"<div class='place'><a href=\"https://{link}\">@{place.strip()}</a></div>")
        else:
            event_html.append(f"<div class='place'>@{location.strip()}</div>")

        event_html = "".join(event_html)
        events_html.append(f"\n<div class='event'>{event_html}</div>")
    return events_html

events = sorted(load_events("dogadjaji.csv"), key=lambda e: e["date"])

today = datetime.today().date()

past_events = list(filter(lambda e: e["date"] <= today, events))
new_events  = list(filter(lambda e: e["date"] >= today, events))


page_template = ""

# Build Serbian Events page
new_events_html = build_html(new_events, DAYS_SR)
with open("pages/sr/events.html", "r") as file:
    page_template = ([line for line in file])[:2]

with open("pages/sr/events.html", "w") as file:
    file.writelines(page_template + new_events_html)

# Build English Events page
new_events_html = build_html(new_events, DAYS_EN)
with open("pages/en/events.html", "r") as file:
    page_template = ([line for line in file])[:2]

with open("pages/en/events.html", "w") as file:
    file.writelines(page_template + new_events_html)

# Build Serbian Archive page
past_events_html = build_html(past_events, DAYS_SR)
with open("pages/sr/events_archive.html", "r") as file:
    page_template = ([line for line in file])[:2]

with open("pages/sr/events_archive.html", "w") as file:
    file.writelines(page_template + past_events_html)

# Build English Archive page
past_events_html = build_html(past_events, DAYS_EN)
with open("pages/en/events_archive.html", "r") as file:
    page_template = ([line for line in file])[:2]

with open("pages/en/events_archive.html", "w") as file:
    file.writelines(page_template + past_events_html)
