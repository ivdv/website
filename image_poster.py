#! /usr/bin/env python3

import argparse
import freetype
import io
from PIL import Image, ImageDraw, ImageFont
import csv
import datetime as dt
from dateutil import relativedelta

CURRENT_TIME = dt.date.today()
NEXT_MONTH = CURRENT_TIME + relativedelta.relativedelta(months=1, day=1)
DAYS_OF_WEEK_SR = ("PON", "UTO", "SRE", "ČET", "PET", "SUB", "NED")
MONTHS_SR = ("Januar", "Februar", "Mart", "April", "Maj", "Jun", "Jul", "Avgust",\
        "Septembar", "Oktobar", "Novembar", "Decembar")

def parseArgs(parser):
    """
        Parse all arguments and return the list of argument values
    """
    parser.add_argument("month", metavar = "MM", help = "two digit number representing the month for which to generate poster", default = "empty", nargs = "?")
    return parser.parse_args()

def load_events(csv_path:str, month:int) -> list[dict]:
    events = []
    with open(csv_path) as csv_file:
        csv_reader = csv.reader(csv_file)
        next(csv_reader, None)
        for event in csv_reader:
            event_date = event[0]
            event_date_parsed = dt.datetime.strptime(event_date, "%d-%m-%Y").date()
            event_time = event[1]
            event_title = event[3]
            current_event = {"date":event_date_parsed,
                             "time":event_time,
                             "title":event_title.strip()}
            if event_date_parsed >= month:
                events.append(current_event)
        return events

def drawPoster(events, bg, fg, month:int):
    fontFacade = ImageFont.truetype('./site/font/Facade-Sud.woff', size=110)
    fontIosevka = ImageFont.truetype('./site/font/iosevka-regular.woff', size=60)
    fontIosevkaSmall = ImageFont.truetype('./site/font/iosevka-regular.woff', size=45)

    W = 1200
    H = 1500 
    img = Image.new('RGB', (W, H), bg)
    draw = ImageDraw.Draw(img)

    header = "DECENTRALA"
    _, _, w, _ = draw.textbbox((0, 0), header, font=fontFacade)
    draw.text(((W-w)/2, 120), header, font=fontFacade, fill=fg)

    subheader = f"Plan za {MONTHS_SR[month.month - 1]}"
    _, _, w, _ = draw.textbbox((0, 0), subheader, font=fontIosevka)
    draw.text(((W-w)/2, 240), subheader, font=fontIosevka, fill=fg)

    height = 410

    draw.text((120, height), "Radionice pocinju u 19h u DC Krovu", font=fontIosevkaSmall, fill=fg)
    height += 100

    for event in events:
        date = DAYS_OF_WEEK_SR[event["date"].weekday()]
        day = event["date"].day
        title = event["title"]
        pad = " " if event["date"].day < 10 else ""
        eventText = f"{date} {day}. {pad} {title}"
        draw.text((120, height), eventText, font=fontIosevkaSmall, fill=fg)
        height += 70

    def drawCircle(x, y):
        r = 10
        draw.ellipse((x - r, y - r, x + r, y+r), fill=fg, outline=(0, 0, 0), width=0)

    LCX = 950 # logo center x
    LCY = 1200 # logo center y
    d = 50 # delta
    drawCircle(LCX - d, LCY)
    drawCircle(LCX, LCY)
    drawCircle(LCX, LCY - d)
    drawCircle(LCX, LCY + d)
    drawCircle(LCX + d, LCY)

    draw.line([(LCX - d, LCY), (LCX + d, LCY)], fill=fg, width=5, joint=None)
    draw.line([(LCX, LCY), (LCX, LCY + d), (LCX + d, LCY), (LCX, LCY - d)], fill=fg, width=5, joint=None)
    draw.text((LCX - 1.7*d, LCY + 1.5*d), "dmz.rs", font=fontIosevka, fill=fg)

    return img

def main():
    # Parse arguments
    parser = argparse.ArgumentParser(description="Generate images of the poster")
    args = parseArgs(parser)

    # Set month based on user input
    month = NEXT_MONTH
    if args.month.isdigit():
      month = dt.date(CURRENT_TIME.year, int(args.month), 1)
    elif args.month != "empty":
      print("Month has to be specified as a number. I will use next month as the default")

    # Load events and draw a poseter
    events = load_events("dogadjaji.csv", month)

    img = drawPoster(events, (0, 0, 0), (20, 250, 50), month)
    img.save('poster_dark.png')

    img = drawPoster(events, (255, 255, 255), (0, 0, 0), month)
    img.save('poster_light.png')

if __name__ == "__main__":
    main()
